package net.tncy.bpe.myproject1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CalculatorTest {

    Calculator c = new Calculator();

    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }   

    @Test
    public void divisionByOne()
    {
        assertEquals(5, c.divise(5, 1));
        assertEquals(10, c.divise(10, 1));
    }   

    @Test
    public void divisionByZero()
    {
        assertEquals(0, c.divise(5, 0));
        assertEquals(0, c.divise(10, 0));
    }   

    @Test
    public void otherDivision()
    {
        assertEquals(3, c.divise(15, 5));
        assertEquals(20, c.divise(100, 5));
    }   
}
